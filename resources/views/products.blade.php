<h1>Все продукты: </h1>
<ul>
@foreach($products as $product)
    <li>
        <a href="/product/{{ $product['id'] }}">{{ $product['title'] }}</a>
        </br>
        <a href="/user/{{ $product['user_id'] }}">Автор продукта</a>
    </li>
@endforeach
</ul>