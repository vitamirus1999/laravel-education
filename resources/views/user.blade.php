<h1>Страница пользователя</h1>
<ul>
	<li>Login: {{ $name }}</li>
	<li>Email: {{ $email }}</li>
	<li>
		Продукты:
		<ul>
			@foreach($products as $product)
				<li>
					<a href="/product/{{ $product['id'] }}">{{ $product['title'] }}</a>
				</li>
			@endforeach
		</ul>
	</li>
</ul>
